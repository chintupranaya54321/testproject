<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\RoleAction $model */

$this->title = Yii::t('app', 'Create Role Action');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Role Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roles' => $roles,
        'all_actions' => $all_actions
    ]) ?>

</div>
