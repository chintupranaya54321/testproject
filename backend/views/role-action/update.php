<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\RoleAction $model */

$this->title = Yii::t('app', 'Update Role Action: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Role Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="role-action-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roles' => $roles,
        'role_action_update' => $role_action_update
    ]) ?>

</div>
