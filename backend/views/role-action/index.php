<?php

use common\models\RoleAction;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var common\models\RoleActionSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Role Actions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-action-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'role_id',
            [
                'attribute' => 'role_id',
                'value' => 'rolename.role_name'
            ],
            // 'all_action_id',
            [
                'attribute' => 'all_action_id',
                'value' => 'actionname.alias_name'
            ],
            // 'created_at',
            // 'updated_at',
            //'is_delete',
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {update}',
                'urlCreator' => function ($action, RoleAction $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id, 'role_id' => $model->role_id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>