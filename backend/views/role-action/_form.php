<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var common\models\RoleAction $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="role-action-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'role_id')->dropDownList($roles, ['prompt' => 'Select'])->label('Role') ?>

    <table class="table table-dark">
        <thead>
            <tr>
                <th>All Actions</th>
                <th>Select</th>
            </tr>
        </thead>
        <tbody class="table table-light">
            <?php if ($all_actions ?? '') { ?>
                <button type="button" name="" class="btn btn-primary" id="check_all" onclick="all_check()">Check All</button>
                <?php foreach ($all_actions as $key => $value) { ?>
                    <tr>
                        <td><?= $value->alias_name ?? '' ?></td>
                        <td><input class="form-check-input" type="checkbox" name="all_action_id[]" value="<?= $value->id ?>"></td>
                    </tr>
                <?php } ?>
            <?php  } ?>
            <?php if ($role_action_update ?? '') {
                foreach ($role_action_update as $k1 => $v1) { ?>
                    <tr>
                        <td><?= $v1->actionname->alias_name ?? '' ?></td>
                        <td><input class="form-check-input" type="checkbox" name="all_action_id[]" <?= $v1->actionname->id ? 'checked' : '' ?> value="<?= $v1->actionname->id ?>"></td>
                    </tr>
            <?php }
            } ?>
        </tbody>
    </table>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function all_check() {
        if ($('#check_all').text() == 'Check All') {
            $('#check_all').html('Uncheck All');
            $('.form-check-input').prop('checked', true);
        } else if ($('#check_all').text() == 'Uncheck All') {
            $('.form-check-input').prop('checked', false);
            $('#check_all').html('Check All');
        }
    }
</script>