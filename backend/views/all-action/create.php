<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\AllAction $model */

$this->title = Yii::t('app', 'Create All Action');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="all-action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
