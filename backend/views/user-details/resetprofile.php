<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'method' => 'POST',
    'action' => ['user-details/resetprofile', 'id' => $id]
]); ?>
<?= $form->field($model, 'password_hash')->textInput(['type' => 'password', 'minlength' => 8, 'required' => true])->label('Reset Password') ?>
<button type="submit" name="reset" value="save" class="btn btn-success">Save</button>
<?php ActiveForm::end(); ?>