<?php

use common\models\UserDetails;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var common\models\UserDetailsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'User Details');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-details-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'user_id',
            [
                'attribute' => 'role_id',
                'value' => 'role.role_name'
            ],
            'name',
            'phone',
            'email:email',
            'address',
            [
                'attribute' => 'gender',
                'value' => function ($model) {
                    return $model->gender == 1 ? 'Male' : ($model->gender == 2 ? 'Female' : 'Other');
                }
            ],
            'dob',
            [
                'attribute' => 'profile_image',
                'format' => 'raw',
                'value' => function ($model) {
                    $img = Yii::getAlias('@storageUrl') . '/profiles/' . $model->profile_image;
                    return '<img src="' . $img . '" width="100" height="100">';
                }
            ],
            [
                'attribute' => 'sign_image',
                'format' => 'raw',
                'value' => function ($model) {
                    $img = Yii::getAlias('@storageUrl') . '/signatures/' . $model->sign_image;
                    return '<img src="' . $img . '" width="100" height="100">';
                }
            ],
            //'created_at',
            //'updated_at',
            //'is_delete',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, UserDetails $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>