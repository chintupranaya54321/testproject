<?php

namespace backend\controllers;

use common\models\AllAction;
use common\models\Role;
use common\models\RoleAction;
use common\models\RoleActionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * RoleActionController implements the CRUD actions for RoleAction model.
 */
class RoleActionController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all RoleAction models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RoleActionSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RoleAction model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RoleAction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new RoleAction();
        $roles = ArrayHelper::map(Role::find()->where(['is_delete' => 0])->andWhere(['!=', 'id', 1])->all(), 'id', 'role_name');
        $all_actions = AllAction::find()->where(['is_delete' => 0])->all();

        if ($this->request->isPost) {
            $role_action_delete = RoleAction::deleteAll(['=', 'role_id', Yii::$app->request->post()['RoleAction']['role_id']]);
            foreach (Yii::$app->request->post()['all_action_id'] as $key => $value) {
                $role_action = new RoleAction();
                $role_action->role_id = Yii::$app->request->post()['RoleAction']['role_id'];
                $role_action->all_action_id = $value;
                $role_action->save();
            }
            return $this->redirect(['index']);
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'roles' => $roles,
            'all_actions' => $all_actions
        ]);
    }

    /**
     * Updates an existing RoleAction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id = '', $role_id = '')
    {
        $model = $this->findModel($id);
        $roles = ArrayHelper::map(Role::find()->where(['is_delete' => 0])->andWhere(['!=', 'id', 1])->all(), 'id', 'role_name');
        $role_action_update = RoleAction::find()->joinWith(['actionname'])->where(['role_id' => $role_id, 'all_action.is_delete' => 0])->all();

        if ($this->request->isPost) {
            $role_action_delete = RoleAction::deleteAll(['role_id' => Yii::$app->request->post()['RoleAction']['role_id']]);
            foreach (Yii::$app->request->post()['all_action_id'] as $key => $value) {
                $role_action = new RoleAction();
                $role_action->role_id = Yii::$app->request->post()['RoleAction']['role_id'];
                $role_action->all_action_id = $value;
                $role_action->save();
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'roles' => $roles,
            'role_action_update' => $role_action_update
        ]);
    }

    /**
     * Deletes an existing RoleAction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RoleAction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return RoleAction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoleAction::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
