<?php

namespace backend\controllers;

use common\models\Action;
use common\models\Role;
use common\models\User;
use common\models\UserDetails;
use common\models\UserDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * UserDetailsController implements the CRUD actions for UserDetails model.
 */
class UserDetailsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        if (Yii::$app->user->identity->role_id == 1) {
            return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
            );
        } else {
            $action_arr = Action::getaccess(Yii::$app->user->identity->role_id ?? 0, Yii::$app->controller->id);
            return [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => $action_arr,
                            'allow' => true
                        ],
                        [
                            'actions' => $action_arr,
                            'allow' => true,
                            'roles' => ['@']
                        ]
                    ]
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ];
        }
    }

    /**
     * Lists all UserDetails models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserDetailsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserDetails model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new UserDetails();
        $user = new User();
        $roles = ArrayHelper::map(Role::find()->where(['is_delete' => 0])->andWhere(['!=', 'id', 1])->all(), 'id', 'role_name');

        if ($this->request->isPost) {
            $username = Yii::$app->request->post()['User']['username'];
            $role_id = Yii::$app->request->post()['UserDetails']['role_id'];
            $email = Yii::$app->request->post()['UserDetails']['email'];
            $password = Yii::$app->request->post()['User']['password_hash'];
            $password = Yii::$app->security->generatePasswordHash($password);
            $user->username = $username;
            $user->role_id = $role_id;
            $user->password_hash = $password;
            $user->email = $email;
            if ($user->save()) {
                if ($model->load($this->request->post())) {
                    $max_user = User::find()->max('id');
                    $model->user_id = $max_user;
                    $profile_image = UploadedFile::getInstance($model, 'profile_image');
                    $profile_image_file = $profile_image->baseName . date('Y-m-d') . $profile_image->extension;
                    $pr_image = Yii::getAlias('@storage') . '/profiles/' . $profile_image_file;
                    $profile_image->saveAs($pr_image);
                    $sign_image = UploadedFile::getInstance($model, 'sign_image');
                    $sign_image_file = $sign_image->baseName . date('Y-m-d') . $sign_image->extension;
                    $sn_image = Yii::getAlias('@storage') . '/signatures/' . $sign_image_file;
                    $sign_image->saveAs($sn_image);
                    $model->profile_image = $profile_image_file;
                    $model->sign_image = $sign_image_file;
                    if ($model->save()) {
                        return $this->redirect(['index']);
                    }
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'roles' => $roles,
            'user' => $user
        ]);
    }

    /**
     * Updates an existing UserDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = new User();
        $roles = ArrayHelper::map(Role::find()->where(['is_delete' => 0])->andWhere(['!=', 'id', 1])->all(), 'id', 'role_name');

        if ($this->request->isPost) {
            if (UploadedFile::getInstance($model, 'profile_image') != '') {
                $profile_image = UploadedFile::getInstance($model, 'profile_image');
                $profile_image_file = $profile_image->baseName . date('Y-m-d') . $profile_image->extension;
                $pr_image = Yii::getAlias('@storage') . '/profiles/' . $profile_image_file;
                $profile_image->saveAs($pr_image);
                $model->profile_image = $profile_image_file;
            }
            if (UploadedFile::getInstance($model, 'sign_image') != '') {
                $sign_image = UploadedFile::getInstance($model, 'sign_image');
                $sign_image_file = $sign_image->baseName . date('Y-m-d') . $sign_image->extension;
                $sn_image = Yii::getAlias('@storage') . '/signatures/' . $sign_image_file;
                $sign_image->saveAs($sn_image);
                $model->sign_image = $sign_image_file;
            } else {
                $model->userdata->username = Yii::$app->request->post()['User']['username'];
                $model->role_id = Yii::$app->request->post()['UserDetails']['role_id'];
                $model->name = Yii::$app->request->post()['UserDetails']['name'];
                $model->phone = Yii::$app->request->post()['UserDetails']['phone'];
                $model->email = Yii::$app->request->post()['UserDetails']['email'];
                $model->address = Yii::$app->request->post()['UserDetails']['address'];
                $model->gender = Yii::$app->request->post()['UserDetails']['gender'];
                $model->dob = Yii::$app->request->post()['UserDetails']['dob'];
                $model->sign_image = $model->sign_image;
                $model->profile_image = $model->profile_image;
            }
            $model->save();
            $model->userdata->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'roles' => $roles,
            'user' => $user
        ]);
    }

    /**
     * Deletes an existing UserDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionResetprofile($id = '')
    {
        $model = new User();
        if (Yii::$app->request->post()) {
            $reset_password = Yii::$app->request->post()['User']['password_hash'];
            $user = User::find()->where(['id' => $id])->one();
            $user->password_hash = Yii::$app->security->generatePasswordHash($reset_password);
            $user->save();
        }

        return $this->render('resetprofile', [
            'model' => $model,
            'id' => $id
        ]);
    }

    protected function findModel($id)
    {
        if (($model = UserDetails::find()->joinWith(['userdata'])->where(['user_details.id' => $id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
