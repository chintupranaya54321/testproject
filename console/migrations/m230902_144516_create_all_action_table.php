<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%all_action}}`.
 */
class m230902_144516_create_all_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%all_action}}', [
            'id' => $this->primaryKey(),
            'controller' => $this->string(255)->notNull(),
            'action' => $this->string(255)->notNull(),
            'alias_name' => $this->string(255),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'),
            'is_delete' => $this->integer()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%all_action}}');
    }
}
