<?php

use yii\db\Migration;

/**
 * Class m230903_105704_add_values_role_table
 */
class m230903_105704_add_values_role_table extends Migration
{
    private $table_name = 'role';
    public function safeUp()
    {
        $roles = [
            ['role_name' => 'Super Admin'],
            ['role_name' => 'Admin'],
            ['role_name' => 'User']
        ];
        foreach ($roles as $value) {
            $this->insert($this->table_name, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230903_105704_add_values_role_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230903_105704_add_values_role_table cannot be reverted.\n";

        return false;
    }
    */
}
