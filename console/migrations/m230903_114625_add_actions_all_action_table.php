<?php

use yii\db\Migration;

/**
 * Class m230903_114625_add_actions_all_action_table
 */
class m230903_114625_add_actions_all_action_table extends Migration
{
    private $table_name = 'all_action';
    public function safeUp()
    {
        $actions = [
            [
                'controller' => 'role',
                'action' => 'index',
                'alias_name' => 'RoleIndex'
            ],
            [
                'controller' => 'role',
                'action' => 'create',
                'alias_name' => 'RoleCreate'
            ],
            [
                'controller' => 'role',
                'action' => 'update',
                'alias_name' => 'RoleUpdate'
            ],
            [
                'controller' => 'role',
                'action' => 'view',
                'alias_name' => 'RoleView'
            ],
            [
                'controller' => 'role',
                'action' => 'delete',
                'alias_name' => 'RoleDelete'
            ],
            [
                'controller' => 'user-details',
                'action' => 'index',
                'alias_name' => 'UserDetailsIndex'
            ],
            [
                'controller' => 'user-details',
                'action' => 'create',
                'alias_name' => 'UserDetailsCreate'
            ],
            [
                'controller' => 'user-details',
                'action' => 'update',
                'alias_name' => 'UserDetailsUpdate'
            ],
            [
                'controller' => 'user-details',
                'action' => 'view',
                'alias_name' => 'UserDetailsView'
            ],
            [
                'controller' => 'user-details',
                'action' => 'delete',
                'alias_name' => 'UserDetailsDelete'
            ]
        ];
        foreach ($actions as $value) {
            $this->insert($this->table_name, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230903_114625_add_actions_all_action_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230903_114625_add_actions_all_action_table cannot be reverted.\n";

        return false;
    }
    */
}
