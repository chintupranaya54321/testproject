<?php

use yii\db\Migration;

/**
 * Class m230903_120505_add_access_role_action_table
 */
class m230903_120505_add_access_role_action_table extends Migration
{
    private $table_name = 'role_action';
    public function safeUp()
    {
        $role_access = [
            [
                'role_id' => 2,
                'all_action_id' => 6
            ],
            [
                'role_id' => 2,
                'all_action_id' => 7
            ],
            [
                'role_id' => 2,
                'all_action_id' => 10
            ],
            [
                'role_id' => 3,
                'all_action_id' => 6
            ],
            [
                'role_id' => 3,
                'all_action_id' => 7
            ],
            [
                'role_id' => 3,
                'all_action_id' => 8
            ]
        ];
        foreach ($role_access as $value) {
            $this->insert($this->table_name, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230903_120505_add_access_role_action_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230903_120505_add_access_role_action_table cannot be reverted.\n";

        return false;
    }
    */
}
