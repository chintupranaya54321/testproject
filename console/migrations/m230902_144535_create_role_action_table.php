<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%role_action}}`.
 */
class m230902_144535_create_role_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%role_action}}', [
            'id' => $this->primaryKey(),
            'role_id' => $this->integer()->notNull(),
            'all_action_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'),
            'is_delete' => $this->integer()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%role_action}}');
    }
}
