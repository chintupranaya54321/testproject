<?php

use yii\db\Migration;

/**
 * Class m230903_110530_add_values_user_user_details_table
 */
class m230903_110530_add_values_user_user_details_table extends Migration
{
    private $user_table = 'user';
    private $user_details_table = 'user_details';
    public function safeUp()
    {
        $user = [
            [
                'username' => 'SuperAdmin',
                'password_hash' => '$2y$13$bjLBTRe5eRe6MFFwV3Q2UOJkaO4x6yiEegaUURZsOn9U2AJxPb3mG',
                'role_id' => 1,
                'email' => 'superadmin@gmail.com',
                'status' => 10
            ],
            [
                'username' => 'Admin',
                'password_hash' => '$2y$13$bjLBTRe5eRe6MFFwV3Q2UOJkaO4x6yiEegaUURZsOn9U2AJxPb3mG',
                'role_id' => 2,
                'email' => 'admin@gmail.com',
                'status' => 10
            ],
            [
                'username' => 'User',
                'password_hash' => '$2y$13$bjLBTRe5eRe6MFFwV3Q2UOJkaO4x6yiEegaUURZsOn9U2AJxPb3mG',
                'role_id' => 3,
                'email' => 'user@gmail.com',
                'status' => 10
            ]
        ];
        $user_details = [
            [
                'user_id' => 1,
                'role_id' => 1,
                'name' => 'Super Admin',
                'phone' => '9874563210',
                'email' => 'superadmin@gmail.com',
                'address' => 'Bhubaneswar',
                'gender' => 1,
                'dob' => '2023-09-02',
                'profile_image' => 'testProfile.png',
                'sign_image' => 'testSignature.png'
            ],
            [
                'user_id' => 2,
                'role_id' => 2,
                'name' => 'Admin',
                'phone' => '1234567898',
                'email' => 'admin@gmail.com',
                'address' => 'Bhubaneswar',
                'gender' => 2,
                'dob' => '2023-08-02',
                'profile_image' => 'testProfile.png',
                'sign_image' => 'testSignature.png'
            ],
            [
                'user_id' => 3,
                'role_id' => 3,
                'name' => 'User',
                'phone' => '1237894562',
                'email' => 'user@gmail.com',
                'address' => 'Bhubaneswar',
                'gender' => 1,
                'dob' => '2023-07-02',
                'profile_image' => 'testProfile.png',
                'sign_image' => 'testSignature.png'
            ]
        ];
        foreach ($user as $user_value) {
            $this->insert($this->user_table, $user_value);
        }
        foreach ($user_details as $user_details_value) {
            $this->insert($this->user_details_table, $user_details_value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230903_110530_add_values_user_user_details_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230903_110530_add_values_user_user_details_table cannot be reverted.\n";

        return false;
    }
    */
}
