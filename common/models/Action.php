<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Action extends Model
{
    public static function getaccess($role_id, $action_alias)
    {
        $all_action_arr = [];
        $all_action = RoleAction::find()->joinWith(['actionname'])->where(['role_id' => $role_id, 'controller' => $action_alias])->all();
        foreach ($all_action as $key => $value) {
            $all_action_arr[] = $value->actionname->action;
        }
        array_push($all_action_arr, 'subcat');
        return $all_action_arr;
    }
}
