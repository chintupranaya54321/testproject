<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserDetails;
use Yii;

/**
 * UserDetailsSearch represents the model behind the search form of `common\models\UserDetails`.
 */
class UserDetailsSearch extends UserDetails
{
    public $role_name;
    public function rules()
    {
        return [
            [['id', 'user_id', 'gender', 'is_delete'], 'integer'],
            [['name', 'phone', 'email', 'address', 'dob', 'created_at', 'updated_at', 'role_id', 'role_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = '';
        if (Yii::$app->user->identity->role_id == 2) {
            $query = UserDetails::find()
                ->joinWith(['role'])
                ->where(['user_details.is_delete' => 0])
                ->andWhere(['!=', 'user_details.role_id', 1]);
        } else if (Yii::$app->user->identity->role_id == 3) {
            $query = UserDetails::find()
                ->joinWith(['role'])
                ->where(['user_details.is_delete' => 0, 'user_details.role_id' => 3]);
        } else {
            $query = UserDetails::find()
                ->joinWith(['role'])
                ->where(['user_details.is_delete' => 0]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            // 'role_id' => $this->role_id,
            'gender' => $this->gender,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'role.role_name', $this->role_name]);

        return $dataProvider;
    }
}
