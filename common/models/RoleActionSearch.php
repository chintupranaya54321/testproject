<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RoleAction;

/**
 * RoleActionSearch represents the model behind the search form of `common\models\RoleAction`.
 */
class RoleActionSearch extends RoleAction
{
    public $role_name, $alias_name;
    public function rules()
    {
        return [
            [['id', 'is_delete'], 'integer'],
            [['created_at', 'updated_at', 'role_id', 'all_action_id', 'role_name', 'alias_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RoleAction::find()->joinWith(['actionname', 'rolename'])->where(['role_action.is_delete' => 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'all_action_id' => $this->all_action_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'rolename.role_name', $this->role_name])
            ->andFilterWhere(['like', 'actionname.alias_name', $this->alias_name]);

        return $dataProvider;
    }
}
