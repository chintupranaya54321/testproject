<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "all_action".
 *
 * @property int $id
 * @property string $controller
 * @property string $action
 * @property string|null $alias_name
 * @property string $created_at
 * @property string|null $updated_at
 * @property int $is_delete
 */
class AllAction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'all_action';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['controller', 'action'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['is_delete'], 'integer'],
            [['controller', 'action', 'alias_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'controller' => Yii::t('app', 'Controller'),
            'action' => Yii::t('app', 'Action'),
            'alias_name' => Yii::t('app', 'Alias Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_delete' => Yii::t('app', 'Is Delete'),
        ];
    }
}
