<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "role_action".
 *
 * @property int $id
 * @property int $role_id
 * @property int $all_action_id
 * @property string $created_at
 * @property string|null $updated_at
 * @property int $is_delete
 */
class RoleAction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role_action';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_id', 'all_action_id'], 'required'],
            [['role_id', 'all_action_id', 'is_delete'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'role_id' => Yii::t('app', 'Role'),
            'all_action_id' => Yii::t('app', 'All Action'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_delete' => Yii::t('app', 'Is Delete'),
        ];
    }

    public function getActionname()
    {
        return $this->hasOne(AllAction::className(), ['id' => 'all_action_id']);
    }

    public function getRolename()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }
}
