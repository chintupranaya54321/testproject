<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="user-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'role_id')->dropDownList($roles, ['prompt' => 'Select', 'required' => true])->label('Role') ?>

    <?= $form->field($model, 'name')->textInput(['required' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['required' => true, 'type' => 'tel', 'onblur' => 'validate_phone(this.value)']) ?>

    <?= $form->field($model, 'email')->textInput(['required' => true, 'type' => 'email']) ?>

    <?= $form->field($user, 'username')->textInput(['required' => $model->userdata ? false : true, 'value' => $model->userdata ? $model->userdata->username : ''])->label('Username') ?>

    <?= $form->field($user, 'password_hash')->textInput(['type' => 'password', 'minlength' => 8, 'required' => $model->userdata ? false : true, 'hidden' => $model->userdata ? true : false])->label($model->userdata ? '' : 'Password') ?>

    <?= $form->field($model, 'address')->textInput(['required' => true]) ?>

    <?= $form->field($model, 'gender')->dropDownList([1 => 'Male', 2 => 'Female', 3 => 'Other'], ['prompt' => 'Select', 'required' => true]) ?>

    <?= $form->field($model, 'dob')->textInput(['type' => 'date', 'required' => true]) ?><br>

    <?php if ($model->profile_image != '') { ?>
        <?= $form->field($model, 'profile_image')->fileInput(['value' => $model->profile_image, 'accept' => '.png, .jpg, .jpeg']) ?>
    <?php } else { ?>
        <?= $form->field($model, 'profile_image')->fileInput(['required' => true, 'accept' => '.png, .jpg, .jpeg']) ?>
    <?php } ?><br>
    <?php if ($model->sign_image) { ?>
        <?= $form->field($model, 'sign_image')->fileInput(['value' => $model->sign_image, 'accept' => '.png, .jpg, .jpeg']) ?>
    <?php } else { ?>
        <?= $form->field($model, 'sign_image')->fileInput(['required' => true, 'accept' => '.png, .jpg, .jpeg']) ?>
    <?php } ?>

    <?php if ($model->userdata) { ?>
        <a href="<?= Url::toRoute(['user-details/resetprofile', 'id' => $model->user_id]) ?>">Forgot or Edit username/password</a>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function validate_phone(phone_no) {
        var pattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (!phone_no.match(pattern)) {
            alert('please enter valid phone number')
            event.preventDefault();
        }
    }
</script>