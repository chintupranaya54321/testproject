<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\UserDetails $model */

$this->title = Yii::t('app', 'Create User Details');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roles' => $roles,
        'user' => $user
    ]) ?>

</div>
